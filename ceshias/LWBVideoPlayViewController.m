//
//  LWBVideoPlayViewController.m
//  ceshias
//
//  Created by apple on 17/6/9.
//  Copyright © 2017年 LWB. All rights reserved.
//
typedef enum LWBPlayState {
    WasOver= 0,
    ManualStop,
} LWBPlayState;

#import "LWBVideoPlayViewController.h"
#import <MobileVLCKit/MobileVLCKit.h>

//协议
@protocol LWBVideoPlayDelegate <NSObject>
@optional
//播放结束时按钮图片的切换
-(void)changePlayBtnState:(LWBPlayState *)state;
@end


@interface LWBVideoPlayViewController ()<VLCMediaPlayerDelegate>
@property(nonatomic,strong)VLCMediaPlayer *player;
//视频盛放的view
@property (weak, nonatomic) IBOutlet UIView *playView;
@property(nonatomic,strong)NSURL *videoURL;
//@property (nonatomic, strong) id<LWBVideoPlayDelegate> delegate;//代理对象
@end

@implementation LWBVideoPlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initViodeView];
    [self setURL:@"http://edge.ivideo.sina.com.cn/6376446.flv?KID=sina,viask&Expires=1497110400&ssig=OtNThHb4AW"];
    [self videoPlay];
    //    UITableView
//    _player.media = [VLCMedia mediaWithURL:[NSURL URLWithString:@"http://edge.ivideo.sina.com.cn/6376446.flv?KID=sina,viask&Expires=1497110400&ssig=OtNThHb4AW"]];
//    _videoURL = [NSURL URLWithString:@"http://edge.ivideo.sina.com.cn/6376446.flv?KID=sina,viask&Expires=1497110400&ssig=OtNThHb4AW"];
}
//-(void)setPlayView:(UIView *)playView{
//   // _playView = playView;
//    [self.view addSubview:playView];
//  ///  NSLog(@"%f,%f,%f,%f",_playView.frame.origin.x,_playView.frame.origin.y,_playView.frame.size.width,_playView.frame.size.height);
//    self.player.drawable = playView;
//}
-(void)initViodeView{
    
    _player = [[VLCMediaPlayer alloc] initWithOptions:nil];
    _player.drawable =_playView;
    _player.media=[VLCMedia mediaWithURL:[NSURL URLWithString:@"http://edge.ivideo.sina.com.cn/6265508.flv?KID=sina,viask&Expires=1497283200&ssig=Fm93rOHyRd"]];
    self.player.delegate =self;
}
//设置url
-(void)setURL:(NSString *)URL{
    _videoURL = [NSURL URLWithString:URL];
}
//播放
-(void)videoPlay{
    if (_videoURL==NULL) {
        ALERTVIEW(@"播放地址URL为空");
        return;
    }else{
        self.player.drawable = _playView;
        self.player.media = [VLCMedia mediaWithURL:_videoURL];
    }
    [self.player play];

}
//暂停播放
-(void)videoPause{
    [self.player pause];
}
//停止播放
-(void)videoStop{
    [self.player stop];
}
//播放状态代理
- (void)mediaPlayerStateChanged:(NSNotification *)aNotification{
    /**
     *  VLCMediaPlayerStateStopped,        //
     < 播放已经停止
     VLCMediaPlayerStateOpening,        //
     < 流是开发的
     VLCMediaPlayerStateBuffering,      //
     < 流正在缓冲
     VLCMediaPlayerStateEnded,          //
     < 流已经结束
     VLCMediaPlayerStateError,          //
     < 播放产生流错误
     VLCMediaPlayerStatePlaying,        //
     < Stream is playing
     VLCMediaPlayerStatePaused          //
     < Stream is paused
     */
        switch (_player.state) {
                //播放完或者手动停止

            case VLCMediaPlayerStateStopped:
            {
                [_player stop];
                [self changePlayBtnState:    WasOver];
            }
                break;
                
            default:
                break;
        }
}
-(void)changePlayBtnState:(LWBPlayState *)state{
    //播放按钮状态 yes播放状态 no暂停状态
    // 判断代理对象是否实现这个方法，没有实现会导致崩溃
//    if ([self.delegate  respondsToSelector:@selector(changePlayBtnState:palyButton:)]) {
//        [self.delegate changePlayBtnState:state];
//    }else{
//        NSLog(@"changePlayBtnState:代理没实现");
//        return;
//    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
