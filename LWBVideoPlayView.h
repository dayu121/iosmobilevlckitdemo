//
//  LWBVideoPlayView.h
//  ceshias
//
//  Created by apple on 17/6/9.
//  Copyright © 2017年 LWB. All rights reserved.
//
typedef enum LWBPlayState {
    WasOver= 0,//播放完成或者手动终止
    ManualStop= 1,//手动终止
    PlayBuffer= 2,//播放缓冲中
    SuspendedToPlay= 3,//被暂停后开始播放
    PlayToSuspended= 4,//播放后被暂停
    PlayError= 5,//播放发生错误
    BufferOpen=6,//缓冲流打开
} LWBPlayState;

#import <UIKit/UIKit.h>
//协议
@protocol LWBVideoPlayDelegate <NSObject>
@required
//播放状态改变时
-(void)changePlayBtnState:(LWBPlayState)state;
//播放时间改变时
-(void)obtainVideoCurrentTime:(int)currentTime;
@optional
//获取视频的缩略图
//获取失败state返回no , 成功则返回 yes 并且返回图片
-(void)obtainHumbnailState:(BOOL )state obtainImage:(UIImage *)image;
//视频开始播放
-(void)videoStartPlay;
//横竖屏切换通知方法(横竖屏切换时可以在该方法做点想做的事)
-(void)SomehowTheScreenSwitch;
//点击手势
-(void)tapGestureRecognizer:(UITapGestureRecognizer *)tap ;
@end

@interface LWBVideoPlayView : UIView
@property(nonatomic,strong)NSString *URL;
@property(nonatomic,strong)NSString *path;
@property (nonatomic, strong) id<LWBVideoPlayDelegate> delegate;//代理对象
@property(nonatomic,strong)NSMutableArray *playVideos;
//自动横竖屏
@property(nonatomic,assign)BOOL isScreensState;
//是否关闭自带的工具栏
@property(nonatomic,assign)BOOL isGiveupToolbar;
//播放
-(void)videoPlay;
//暂停播放
-(void)videoPause;
//停止播放
-(void)videoStop;
//获取视频总时长(必须在视频开始播放时才能获取到总时长)
-(int)obtainVideoTotalTime;
-(int)obtainVideoRemainingTime;//获取视频剩余时间
//-(void)obtainHumbnail:(void (^)(UIImage * _Nullable image))okHumbnail obtainHumbnailError:(void (^) (NSString * error))error;
//获取缩略图(必须在视频开始播放时才能获取成功)
-(void)obtainHumbnail;
//设置视频的播放速度(正常速度是1.0)
-(void)setUpPlaySpeed:(float)speed;
//设置当前从哪里开始播放（0.0-1.0  必须在视频开始播放后设置才能成功）
-(void)setUpCurrentProgress:(float)progress;
//获取当前播放位置
-(float)obtainCurrentProgress;
//下载当前视频
//参数：type视频的格式
-(NSString *)downloadCurrentVideoType:(NSString *)type;
//下载视频
-(void)downloadVideoURL:(NSString *)url ofType:(NSString *)type Results:(void (^)(NSString  * path,BOOL state))results;
//读取已下载保存的视频文件,因为视频保存在Directories文件夹下的Video文件夹里面。所以得到的路径要加上video文件夹的路径才能访问。
-(NSMutableArray *)readVideoFolder;
//设置全屏播放
-(void)setUpFullscreenPlay;
//设置音量的值(范围0.0-1.0)
-(void)setUpVolume:(float)value;
//设置屏幕亮度(范围0.0-1.0)
-(void)setScreenBrightness:(float)value;
//向前快进一段视频
-(void)forwardJumpingProgressVideo;
//向前快进指定秒数视频
-(void)forwardJumpingSpecifiedTimeVideo:(int)time;
//向后快进指定秒数视频
-(void)backwardJumpingSpecifiedTimeVideo:(int)time;
//向后快进一段视频
-(void)backwardJumpingProgressVideo;
//强制横屏
-(void)mandatoryLandscape;
//强制竖屏
-(void)mandatoryVerticalScreen;
@end
